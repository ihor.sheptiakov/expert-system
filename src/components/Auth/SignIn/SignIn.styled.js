import styled from "styled-components";
import {color} from "../../../config";

export const SignInWrapper = styled.div`
  gap: 1vh;
  width: 100%;
  display: flex;
  flex-direction: column;
`

export const SignInWrapperInput = styled.input`
  outline: none;
  padding: 1vh 1vw;

  /* Chrome, Safari, Edge, Opera */
  &::-webkit-outer-spin-button,
  &::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  /* Firefox */
  &[type=number] {
    -moz-appearance: textfield;
  }
`

export const SignInWrapperSubmit = styled.button`
  border: none;
  outline: none;
  cursor: pointer;
  padding: 1vh 1vw;
  font-weight: bold;
  color: ${color.buttonText};
  background-color: ${color.buttonBackground};
`