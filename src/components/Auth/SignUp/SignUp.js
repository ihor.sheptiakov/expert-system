import React, {useContext, useState} from "react";
import {
    SignUpWrapper,
    SignUpWrapperSubmit,
    SignUpWrapperInput
} from './SignUp.styled'
import {AuthContext} from "../../../store/store";
import {apiFetchSignUp} from "../../../api/auth";

function SignUp() {
    const [name, setName] = useState('')
    const [login, setLogin] = useState('')
    const [password, setPassword] = useState('')
    const [authData, setAuthData] = useContext(AuthContext)

    const signUpHandler = () => {
        apiFetchSignUp(name, login, password)
            .then(isSuccess => {
                if (!isSuccess) {
                    alert('User already exists or invalid credentials.')
                } else {
                    setAuthData({
                        ...authData,
                        signInStatus: isSuccess
                    })
                }
            })
    }

    return (
        <SignUpWrapper>
            <SignUpWrapperInput
                placeholder={'Name'}
                value={name}
                onChange={e => setName(e.target.value)}
            />
            <SignUpWrapperInput
                type={'email'}
                placeholder={'E-mail'}
                value={login}
                onChange={e => setLogin(e.target.value)}
            />
            <SignUpWrapperInput
                type={'password'}
                placeholder={'Password'}
                value={password}
                onChange={e => setPassword(e.target.value)}
            />
            <SignUpWrapperSubmit onClick={signUpHandler}>
                Sign Up
            </SignUpWrapperSubmit>
        </SignUpWrapper>
    )
}

export default SignUp