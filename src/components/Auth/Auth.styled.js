import styled from "styled-components";

export const AuthWrapper = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
`

export const AuthWrapperTitle = styled.h2``

export const AuthWrapperWindow = styled.div`
  gap: 10px;
  display: flex;
  margin-top: 5vh;
  padding: 2vh 5vw;
  align-items: center;
  flex-direction: column;
`

export const AuthWrapperWindowSwitch = styled.p`
  font-size: 1rem;
  
  & b {
    cursor: pointer;
  }
`