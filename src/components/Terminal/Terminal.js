import React from "react";
import {
    AppWrapperTerminal,
    AppWrapperTerminalHeader,
    AppWrapperTerminalHeaderIcon,
    AppWrapperTerminalHeaderTitle
} from "../../App.styled";
import ArrowSVG from "../../assets/arrow.svg";

function Terminal({logCounter, consoleStatus, consoleStatusHandler, setLog}) {
    return (
        <AppWrapperTerminal>
            <AppWrapperTerminalHeader>
                <AppWrapperTerminalHeaderTitle>
                    {'Console'} {
                    !logCounter ||
                    <div>{logCounter}</div>
                }
                </AppWrapperTerminalHeaderTitle>
                <AppWrapperTerminalHeaderIcon
                    src={ArrowSVG}
                    alt={'Arrow'}
                    rotate={consoleStatus}
                    onClick={consoleStatusHandler}
                />
            </AppWrapperTerminalHeader>
            {
                consoleStatus &&
                setLog('warning', 'Error', 'Message')
            }
        </AppWrapperTerminal>
    )
}

export default Terminal