import React, {useContext, useRef, useState} from "react";
import {TabsWrapperTab, TabsWrapperTabClose, TabsWrapperTabIcon, TabsWrapperTabTitle} from "./Tabs.styled";
import DBColorIcon from "../../assets/db-icon-color.png";
import CloseIcon from "../../assets/close.svg";
import {ExpertSystemsListContext} from "../../store/store";
import TabMenu from "./TabMenu/TabMenu";

function Tab({list, listIndex, es}) {
    const tabRef = useRef(null)
    const [expertSystemsList, setExpertSystemsList] = useContext(ExpertSystemsListContext)
    const [menuStatus, setMenuStatus] = useState(false)

    const tabHandler = (id) => {
        if (!menuStatus) {
            const filteredList = list
            ?.map((item) => ({
                ...item,
                active: id === item?.id
            }))
            setExpertSystemsList(filteredList)
        }
    }

    const tabMenuHandler = (e) => {
        e?.preventDefault()
        setMenuStatus(!menuStatus)
    }

    const closeTabHandler = (e, id) => {
        e.stopPropagation()
        const filteredList = list?.map(item => item.id === id ? {
            ...item,
            closed: true,
        } : item)

        setMenuStatus(false)
        setExpertSystemsList(filteredList)
    }

    return (
        <TabsWrapperTab
            key={es?.id}
            ref={tabRef}
            menuStatus={menuStatus}
            activeTabStatus={es?.active}
            onClick={() => tabHandler(es?.id)}
            onContextMenu={(e) => tabMenuHandler(e, es)}
        >
            {
                menuStatus &&
                <TabMenu
                    top={tabRef?.current?.offsetHeight}
                    left={tabRef?.current?.getBoundingClientRect()?.left}
                    width={tabRef?.current?.offsetWidth}
                    es={es}
                    listIndex={listIndex}
                    tabMenuHandler={tabMenuHandler}
                />
            }
            <TabsWrapperTabIcon
                src={DBColorIcon}
                alt={'Database icon'}
            />
            <TabsWrapperTabTitle
                title={es?.title}
            >
                {es?.title?.slice(0, 12)}...
            </TabsWrapperTabTitle>
            <TabsWrapperTabClose
                activeTabStatus={es?.active}
                src={CloseIcon}
                alt={'Close'}
                onClick={(e) => closeTabHandler(e, es?.id)}
            />
        </TabsWrapperTab>
    )
}

export default Tab