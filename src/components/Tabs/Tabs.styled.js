import styled from "styled-components";
import {color} from "../../config";

export const TabsScrollWrapper = styled.div`
  width: 100%;
  position: relative;
  border: 1px solid ${color.border};
`

export const TabsWrapper = styled.div`
  width: 100%;
  display: grid;
  overflow-x: auto;
  align-items: center;
  grid-template-columns: repeat(10, 1fr);
`

export const TabsWrapperTab = styled.div`
  gap: .25rem;
  height: 100%;
  display: flex;
  padding: 1rem;
  cursor: pointer;
  //position: relative;
  align-items: center;
  background-color: ${p => p.activeTabStatus ? color.tabBackground : ''};
  border-bottom: 3px solid ${p => p.activeTabStatus ? color.tabBorder : 'transparent'};
  
  &:hover {
    background-color: ${p => p.activeTabStatus ? '' : p.menuStatus ? '' : color.tabBackgroundHover};
    
    & img {
      filter: ${p => p.activeTabStatus ? '' : p.menuStatus ? '' : 'brightness(0)'};
    }
  }
`

export const TabsWrapperTabIcon = styled.img`
  width: 1rem;
  height: 1rem;
`

export const TabsWrapperTabTitle = styled.p`
`

export const TabsWrapperTabClose = styled.img`
  width: 1rem;
  height: 1rem;
  padding: .25rem;
  margin: 0 0 0 auto;
  border: 1px solid;
`

export const TabsWrapperTabCreate = styled(TabsWrapperTabClose)`
  cursor: pointer;
  margin: -3px 0 0 5px;
  transform: rotate(45deg);
`