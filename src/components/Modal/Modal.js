import React, {useEffect, useRef} from 'react'
import {
    ModalWrapper,
    ModalWrapperWindow,
    ModalWrapperWindowBody,
    ModalWrapperWindowHeader,
    ModalWrapperWindowHeaderClose,
    ModalWrapperWindowHeaderTitle,
} from "./Modal.styled";
import Close from '../../assets/close.svg'

function Modal(props) {
    const {
        title,
        body,
        setModalStatus,
    } = props
    const modalRef = useRef(null)
    const modalWindowRef = useRef(null)

    const closeModalHandler = () => setModalStatus(false)

    useEffect(() => {
        //Here is the listener which listen to our click by clicking outside the modal and if we click outside it will work like a closing and repeat all actions
        function handleClickOutside(event) {
            const target = event.target
            if (
                modalRef?.current?.contains(target) &&
                !modalWindowRef?.current?.contains(target)
            ) {
                closeModalHandler()
            }
        }

        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [modalRef, modalWindowRef]);

    return (
        <ModalWrapper
            ref={modalRef}
        >
            <ModalWrapperWindow
                ref={modalWindowRef}
            >
                <ModalWrapperWindowHeader>
                    <ModalWrapperWindowHeaderTitle>
                        {title}
                    </ModalWrapperWindowHeaderTitle>
                    <ModalWrapperWindowHeaderClose
                        src={Close}
                        onClick={closeModalHandler}
                    />
                </ModalWrapperWindowHeader>
                <ModalWrapperWindowBody>
                    {body}
                </ModalWrapperWindowBody>
            </ModalWrapperWindow>
        </ModalWrapper>
    )
}

export default Modal