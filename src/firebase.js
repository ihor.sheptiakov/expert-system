// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth } from 'firebase/auth'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyCv-cKwM4Q5w6NvY4Y0s9lPAv4VLjHijZI",
    authDomain: "expert-system-ae417.firebaseapp.com",
    projectId: "expert-system-ae417",
    storageBucket: "expert-system-ae417.appspot.com",
    messagingSenderId: "287267034417",
    appId: "1:287267034417:web:f20a878d76c9b5c2358c10",
    measurementId: "G-QS8VEHWN1F"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
const auth = getAuth(app)
export default auth
