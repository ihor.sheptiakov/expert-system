import axios from "axios";
import {apiUrl} from "../config";

export const apiFetchLogin = (email, password) => {
    return axios.post(
        `${apiUrl}auth/login`,
        {email, password}
    ).then(({data}) => {
        if (data) {
            const {user, tokens} = data?.data
            const {accessToken, refreshToken} = tokens
            localStorage.setItem('accessToken', accessToken)
            localStorage.setItem('refreshToken', refreshToken)
            return user
        }
    }).catch(() => false)
}

export const apiFetchSignUp = (name, email, password) => {
    return axios.post(
        `${apiUrl}auth/signup`,
        {name, email, password}
    ).then(() => true)
        .catch(() => false)
}

export const apiFetchRefreshToken = () => {
    return axios.post(
        `${apiUrl}auth/refresh`,
        {refreshToken: localStorage.getItem('refreshToken')}
    ).then(({data: response}) => {
        if (response) {
            const {accessToken, refreshToken} = response.data
            localStorage.setItem('accessToken', accessToken)
            localStorage.setItem('refreshToken', refreshToken)
            return accessToken
        }
    }).catch(() => false)
}