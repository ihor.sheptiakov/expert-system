import {createContext} from "react";

export const AuthContext = createContext(null)

export const ExpertSystemsListContext = createContext([])

export const SplitterContext = createContext(null)

export const RenameFileContext = createContext(null)